#!/bin/bash

# Copyright (c) 2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

export PATH=`pwd`/tpc_resource/CITools/js:$PATH

source check_env.sh
OHOS_BASE_SDK_HOME=
OHPM_FILE_DIR=$ROOT_DIR/ohpm/bin
HAP_FILE_DIR=$ROOT_DIR/out/tpc
HOS_SDK_HOME=
function check_prepare() {
    if [ ! -d $OHOS_SDK_HOME ]
    then
        echo "SDK not prepare! start prepare.sh first!"
        return 1
    fi
    export OHOS_BASE_SDK_HOME="$OHOS_SDK_HOME"
    export HOS_SDK_HOME="$OHOS_SDK_HOME"
    if [ ! -d $NODE_HOME ]
    then
        echo "Nodejs not prepare! start prepare.sh first!"
        return 1
    fi
    if [ ! -d $OHPM_FILE_DIR ]
    then
        echo "ohpm not prepare! start prepare.sh first!"
        return 1
    fi
    export NODE_HOME=${NODE_HOME}
    export PATH=$NODE_HOME/bin:$PATH
    export PATH=$PATH:$OHOS_SDK_HOME:$OHPM_FILE_DIR:$NODE_HOME:$OHOS_BASE_SDK_HOME

    return 0
}

function check_out_dir() {
    if [ ! -d $HAP_FILE_DIR ]
    then
        mkdir -p $HAP_FILE_DIR
    fi
}

function ohpm_init() {
    echo "ohpm init!"
    $NODE_HOME/bin/npm config set @ohos:registry=https://repo.harmonyos.com/npm/
    $NODE_HOME/bin/npm config set lockfile=false

    cd $OHPM_FILE_DIR
    bash init
    cd $OLDPWD
}

function build_init() {
    echo "build_init ########"
    if [ -z "$COMPONT_NAME" ]
    then
        echo "no compont to build!"
        return 1
    fi

    if [ -e "$COMPONT_NAME/.gitmodules" ]
    then
        echo "***********check gitmodules************"
        for submodule_path in `cat $COMPONT_NAME/.gitmodules | grep "path ="`
        do
            if [[ $submodule_path == "path" ]] || [[ $submodule_path == "=" ]]
            then
                continue
            fi
            if [ ! -d "$COMPONT_NAME/$submodule_path" ]
            then
                echo "get submodules path: $COMPONT_NAME/$submodule_path failed!"
                return 1
            fi
            cd $COMPONT_NAME/$submodule_path
            git submodule update --init --recursive
            cd $OLDPWD
        done
    fi

    return 0
}

function start_build() {
    echo "CI start compile**********************************"
    echo "OHOS_BASE_SDK_HOME=$OHOS_BASE_SDK_HOME"
    echo "OHOS_SDK_HOME=$OHOS_SDK_HOME"

    local res=0
    if [ -z $COMPONT_NAME ]
    then
        echo "have not get the compont!"
        return 1
    fi

    if [ $COMPONT_NAME == "openharmony_tpc_samples" ]
    then
        echo "not need to compile openharmony_tpc_samples!!!"
        return 0
    fi
    CURWORK_DIR=$ROOT_DIR/$COMPONT_NAME
    cd $CURWORK_DIR

    if [ -e "oh-package.json5" ]
    then
        for path in $(find . -name "oh-package.json5")
        do
            path_array=(${path//\// })
            lenth=${#path_array[@]}
            if [ $lenth -eq 3 ]
            then
                cd ${path_array[1]}
                ohpm install
                cd $OLDPWD
            fi
        done
        ohpm install
        bash ./hvigorw --mode module assembleHap --no-daemon
        res=$?
    elif [ -e "package.json" ]
    then
        for path in $(find . -name "package.json")
        do
            path_array=(${path//\// })
            lenth=${#path_array[@]}
            if [ $lenth -eq 3 ]
            then
                cd ${path_array[1]}
                $NODE_HOME/bin/npm install
                cd $OLDPWD
            fi
        done
        $NODE_HOME/bin/npm install
        echo "NODE_HOME:$NODE_HOME"
        $NODE_HOME/bin/node $CURWORK_DIR/node_modules/@ohos/hvigor/bin/hvigor.js --mode module assembleHap
        res=$?
    else
        echo "can not support this project! miss package.json or oh-package.json5"
        res=1;
    fi

    if [[ $res -eq 0 ]] && [[ -e "$CURWORK_DIR/entry/build/default/outputs/default/entry-default-unsigned.hap" ]]
    then
        echo "************compile success****************"
        cp -rf $CURWORK_DIR/entry/build/default/outputs $HAP_FILE_DIR
    else
        res=1
        echo "************compile fail****************"
    fi

    return $res
}

function compile() {
    get_apiversion $1
    if [ -z $API_VERSION ]
    then
        echo "no need to compile!!"
        return 0
    fi
    check_prepare
    if [ $? -ne 0 ]
    then
        echo "############################ check_prepare failed ############################"
        return 1
    fi
    check_out_dir
    ohpm_init
    build_init
    if [ $? -ne 0 ]
    then
        echo "############################ build_init failed ############################"
        return 1
    fi
    start_build
    if [ $? -ne 0 ]
    then
        echo "############################ start_build failed ############################"
        return 1
    fi

    return 0
}

compile $*
exit $?

#eof
